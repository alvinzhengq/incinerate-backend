# Incinerate

### Hidden Service URL (No Longer Available):
3mghupyalwu7gub3ncpe3tcynf54y2bliylnh6gbslrlib4liwsqlgyd.onion


### How to Run
1. Install Redis on your host, and change the config file to listen on all interfaces.
2. Install docker
3. Clone the repo, cd in and run docker build . -t incinerate
4. docker volume create file-storage
5. docker run -d -v file-storage:/filestorage -p 4000:80 --add-host=host.docker.internal:host-gateway --name incinerate incinerate

Website is now accessible at localhost:80

### Details:

The backend of Incinerate uses mat2 for metadata stripping which is the recommended metadata stripping tool by the Tor Project people, and comes preinstalled on Tails OS, so it is one of the best tools for metadata stripping out there. Once stripped the file will be stored on the server for maximum of 3 minutes, and can be downloaded at any time within those 3 minutes. But as soon as the file is downloaded it will be deleted regardless of whether the 3 minutes are up or not to provide maximum privacy.
